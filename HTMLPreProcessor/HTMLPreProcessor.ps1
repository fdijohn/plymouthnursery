﻿
# REQUIRED Statement
Clear-Host

# REQUIRED Statement
$ErrorActionPreference = 'Stop'

#----------------------------------------------------------------------------------------------------------------------------------
# Main program begins here
#----------------------------------------------------------------------------------------------------------------------------------
# Remove-Module ProcLibrary  # --- Must be commented out before running in production only used for testing
Import-Module ProcLibrary

# Get the name of the directory where the script is runing from
$strScriptDir = $PSScriptRoot

####   Get-ChildItem "C:\inetpub\wwwroot\NewPlymouthNurserySite" -Recurse | Where-Object LastWriteTime -gt ([DateTime]::Parse('2017/04/19 00:00:00'))

# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter *.phtml
# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter index.phtml
# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter lighting.phtml
# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter landscaping.phtml
# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter commercial.phtml
# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter holidaydecorating.phtml
# $PHTMLFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\' -Filter pondsandwaterfalls.phtml

foreach($PHTMLFile in $PHTMLFiles)
{
#    if ($PHTMLFile.Name -eq 'pondsandwaterfalls.phtml') {
        write-host 'Processing: ' ($PHTMLFile.Name)

        $FileContents = ''

        $reader = [System.IO.File]::OpenText('C:\inetpub\wwwroot\NewPlymouthNurserySite\' + $PHTMLFile.Name)

        while (($line = $reader.ReadLine()) -ne $null)
        {
            if (($line -match 'INSERT-HTML-FILE') -eq $True) {
                $IndexOfColon = $line.IndexOf(':')
                $File2InsertName = $line.Substring($IndexOfColon + 1).Trim()
                $Code2Insert = Get-Content ('C:\inetpub\wwwroot\NewPlymouthNurserySite\' + $File2InsertName)

                # Make the INSERT-HTML-FILE line a comment in the generated HTML file
                $IndexOfInsert = $line.IndexOf('INSERT-HTML-FILE')
                $CommentLine = (''.PadRight($IndexOfInsert, ' ')) + '<!-- ' + $line.Trim() + ' -->'
                $FileContents += ($CommentLine + ("`r`n"))

                foreach ($CodeLine in $Code2Insert) {
                    $FileContents += ($CodeLine + ("`r`n"))
                }
            
                # Jump to While Loop w/o processing the INSERT-HTML-FILE line

                continue
            }

            $FileContents += ($line + ("`r`n"))
        }

        $reader.Close()

        $writer = [System.IO.File]::WriteAllText('C:\inetpub\wwwroot\NewPlymouthNurserySite\' + $PHTMLFile.BaseName +  '.html', $FileContents)
    #}
}
