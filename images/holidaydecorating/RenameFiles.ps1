# REQUIRED Statement
Clear-Host

# REQUIRED Statement
$ErrorActionPreference = 'Stop'

$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
# Write-host "My directory is $dir"

#$PondFiles = Get-ChildItem -Path 'C:\inetpub\wwwroot\NewPlymouthNurserySite\images\pondsandwaterfalls\' -Filter Ponds*.jpg

$Files = Get-ChildItem -Path $dir -Filter holidaydecorating*.jpg | Sort -Property Name -Descending

$dir = $dir + '\'

foreach($File in $Files){
    $underscore_position = $File.Name.IndexOf('_')
    [int] $picture_number = $File.Name.Substring($underscore_position + 1, 2)
    $new_picture_number = $picture_number + 1
    $new_file_name = $File.Name.Substring(0,18) + $new_picture_number.ToString('00') + '.jpg'
    Write-host $File.Name "`t" $new_file_name
    Write-Host 
    rename-item ($dir + $File.Name) ($dir + $new_file_name)
    # Write-host $File $File.Name.ToLower().replace('kitchensfireplaces','outdoorlivingspaces')
    # rename-item  ($dir+$File) ($dir+$File.Name.ToLower().replace('kitchensfireplaces','outdoorlivingspaces'))
}
